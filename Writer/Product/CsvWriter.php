<?php

namespace Naolis\Bundle\ConnectorBundle\Writer\Product;

use Akeneo\Bundle\BatchBundle\Job\RuntimeErrorException;

class CsvWriter extends TableWriter
{
    /**
     * {@inheritdoc}
     */
    public function flush()
    {
        $this->writtenFiles[$this->getPath()] = basename($this->getPath());

        $uniqueKeys = $this->columns;
        if (false === $csvFile = fopen($this->getPath(), 'w')) {
            throw new RuntimeErrorException('Failed to open file %path%', ['%path%' => $this->getPath()]);
        }

        $header = $this->isWithHeader() ? $uniqueKeys : [];
        if (false === fputcsv($csvFile, $header, $this->delimiter)) {
            throw new RuntimeErrorException('Failed to write to file %path%', ['%path%' => $this->getPath()]);
        }

        $page = 0;
        while (count($rows = $this->getRows($page++))) {
            foreach ($rows as $row) {
                if (false === fputcsv($csvFile, $row, $this->delimiter, $this->enclosure)) {
                    throw new RuntimeErrorException('Failed to write to file %path%', ['%path%' => $this->getPath()]);
                } elseif ($this->stepExecution) {
                    $this->stepExecution->incrementSummaryInfo('write');
                }
            }
        }

        $this->schemaManager->dropTable($this->table);
    }

    /**
     * Fetch rows from export table.
     *
     * @param int $page
     *
     * @return array
     */
    protected function getRows($page)
    {
        return $this->connection->query(sprintf(
            'SELECT * FROM %s LIMIT %d OFFSET %d',
            $this->table,
            100,
            $page * 100
        ))->fetchAll();
    }
}
