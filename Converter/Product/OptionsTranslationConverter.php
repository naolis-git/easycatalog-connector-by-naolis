<?php

namespace Naolis\Bundle\ConnectorBundle\Converter\Product;

use Pim\Bundle\CatalogBundle\Entity\AttributeOption;
use Pim\Bundle\CatalogBundle\Entity\AttributeOptionValue;
use Pim\Bundle\CatalogBundle\Entity\Channel;
use Pim\Bundle\CatalogBundle\Model\Product;
use Pim\Bundle\CatalogBundle\Model\ProductValue;

class OptionsTranslationConverter
{
    /**
     * @param Product $product
     * @param Channel $channel
     */
    public function convert(Product $product, Channel $channel)
    {
        foreach ($product->getValues() as $value) {
            /* @var $value ProductValue */
            if ($value->getAttribute()->getBackendType() == 'option' && $value->getOption()) {
                $translations = array();
                foreach ($value->getOption()->getOptionValues() as $optValue) {
                    /* @var $optValue AttributeOptionValue */
                    if (in_array($optValue->getLocale(), $channel->getLocaleCodes())) {
                        $trans = $optValue->getLabel() ? $optValue->getLabel() : $value->getOption()->getCode();
                        $translations[$optValue->getLocale()] = $trans;
                    }
                }
                $option = new AttributeOption;
                $option->setCode($translations);
                $value->setData($option);
            }
        }
    }
}
