<?php

namespace Naolis\Bundle\ConnectorBundle\Sorter;

/**
 * Interface SorterInterface
 *
 * @author Aymeric Morilleau <am@naolis.com>
 * @copyright 2015 Naolis SARL (http://www.naolis.com)
 * @license   http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 */
interface SorterInterface
{
    /**
     * @param $data
     * @param $context
     * @return mixed
     */
    public function sort($data, $context);
}
