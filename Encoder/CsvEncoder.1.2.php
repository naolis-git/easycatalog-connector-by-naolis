<?php

namespace Naolis\Bundle\ConnectorBundle\Encoder;

use Symfony\Component\Serializer\Encoder\EncoderInterface;
use Naolis\Bundle\ConnectorBundle\Sorter\SorterInterface;

/**
 * CSV Encoder for Naolis specific needs
 * Forked from Akeneo CSV Encoder to can :
 * - sort lines with Sort service
 * - expand in multiple lines for one item
 * - expand in multiple lines for each locale
 * - expand in multiple columns if column needs
 * - remove locale and channel from header entry
 *
 * @author Aymeric Morilleau <am@naolis.com>
 * @copyright 2015 Naolis SARL (http://www.naolis.com)
 * @license   http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 */
class CsvEncoder implements EncoderInterface
{
    /**
     * @staticvar string
     */
    const FORMAT = 'ncsv';
    /**
     * @var boolean
     */
    protected $firstExecution = true;

    /**
     * @var boolean
     */
    protected $hasHeader      = false;

    /**
     * @var string
     */
    protected $delimiter;

    /**
     * @var string
     */
    protected $enclosure;

    /**
     * @var string
     */
    protected $withHeader;

    /**
     * @var string
     */
    protected $cleanHeader;

    /**
     * @var SorterInterface
     */
    protected $sorter;

    /**
     * {@inheritdoc}
     */
    public function supportsEncoding($format)
    {
        return self::FORMAT === $format;
    }

    /**
     * {@inheritdoc}
     */
    public function encode($data, $format, array $context = array())
    {
        if (!is_array($data)) {
            throw new \InvalidArgumentException(
                sprintf('Expecting data of type array, got "%s".', gettype($data))
            );
        }

        $this->initializeContext($context);

        $output = fopen('php://temp', 'r+');

        $first = reset($data);
        if (isset($first) && is_array($first)) {

            // Expand line with array in multiple lines
            $data = $this->expandLines($data);

            // Expand line with one locale a line
            $data = $this->expandLocalizedFieldsLines($data, $context);

            // Sort lines if sorter is defined
            if ($this->sorter) {
                $data = $this->sorter->sort($data, $context);
            }

            // Expand column with '%d' in header item as multiple columns
            $data = $this->expandColumns($data);

            $columns = $this->getColumns($data);
            if ($this->withHeader && !$this->hasHeader) {
                $this->encodeHeader($columns, $output, $this->delimiter, $this->enclosure, $context);
            }

            foreach ($this->normalizeColumns($data, $columns) as $entry) {
                $this->checkHasStringKeys($entry);
                $this->write($output, $entry, $this->delimiter, $this->enclosure);
            }
        } else {
            if ($this->withHeader && !$this->hasHeader) {
                $this->encodeHeader($data, $output, $this->delimiter, $this->enclosure, $context);
            }
            $this->write($output, $data, $this->delimiter, $this->enclosure);
        }

        return trim($this->readCsv($output));
    }

    /**
     * Initialize CSV encoder context merging default configuration
     *
     * @param array $context
     *
     * @throws \RuntimeException
     */
    protected function initializeContext(array $context)
    {
        $context = array_merge($this->getDefaultContext(), $context);

        if (!$this->firstExecution && $context['heterogeneous']) {
            throw new \RuntimeException(
                'The csv encode method should not be called more than once when handling heterogeneous data. '.
                'Otherwise, it won\'t be able to compute the csv columns correctly.'
            );
        }
        $this->firstExecution = false;

        $this->delimiter   = is_string($context['delimiter']) ? $context['delimiter'] : ';';
        $this->enclosure   = is_string($context['enclosure']) ? $context['enclosure'] : '"';
        $this->withHeader  = is_bool($context['withHeader']) ? $context['withHeader'] : false;
        $this->sorter      = isset($context['sorter']) ? $context['sorter'] : false;
    }

    /**
     * Get a default context for the csv encoder
     *
     * @return array
     */
    protected function getDefaultContext()
    {
        return array(
            'delimiter'     => ';',
            'enclosure'     => '"',
            'withHeader'    => false,
            'heterogeneous' => false
        );
    }

    /**
     * @param array  $data
     * @param mixed  $output
     * @param string $delimiter
     * @param string $enclosure
     * @param array  $context
     */
    private function encodeHeader($data, $output, $delimiter, $enclosure, $context)
    {
        $headers = array_keys($data);

        $this->write($output, $headers, $delimiter, $enclosure);
        $this->hasHeader = true;
    }

    /**
     * @param mixed  $output
     * @param array  $entries
     * @param string $delimiter
     * @param string $enclosure
     */
    private function write($output, $entries, $delimiter, $enclosure)
    {
        fputcsv($output, $entries, $delimiter, $enclosure);
    }

    /**
     * @param mixed $csvResource
     *
     * @throws \Exception
     * @return string
     */
    private function readCsv($csvResource)
    {
        rewind($csvResource);

        return stream_get_contents($csvResource);
    }

    /**
     * @param array $data
     * @param array $columns
     *
     * @return array
     */
    private function normalizeColumns(array $data, array $columns)
    {
        foreach ($data as $key => $item) {
            $data[$key] = array_merge($columns, $item);
        }

        return $data;
    }

    /**
     * @param array $data
     *
     * @return array
     */
    private function getColumns(array $data)
    {
        $columns = array();

        foreach ($data as $item) {
            foreach (array_keys($item) as $key) {
                $columns[$key] = '';
            }
        }

        return $columns;
    }

    /**
     * @param array $data
     *
     * @throws \InvalidArgumentException
     */
    private function checkHasStringKeys(array $data)
    {
        if (empty($data)) {
            return;
        }

        foreach (array_keys($data) as $key) {
            if (!is_string($key)) {
                throw new \InvalidArgumentException(
                    sprintf('Expecting keys of type "string" but got "%s".', gettype($key))
                );
            }
        }
    }

    /**
     * @param array $entries
     * @return array
     */
    private function expandLines($entries)
    {
        $result = array();
        foreach ($entries as $entry) {
            $result = array_merge($result, $this->expandLine($entry));
        }
        return $result;
    }

    /**
     * @param array $entries
     * @return array
     */
    private function expandLine($entries)
    {
        $fields = array();
        foreach ($entries as $key => $entry) {
            if (is_array($entry)) {
                $fields[] = $key;
            }
        }
        $data = array($entries);

        if (count($fields) == 0) {
            return $data;
        }

        foreach ($fields as $field) {
            $result = array();
            foreach ($entries[$field] as $value) {
                foreach ($data as $item) {
                    $item[$field] = $value;
                    $result[] = $item;
                }
            }
            $data = $result;
        }

        return $data;
    }

    /**
     * @param $entries
     * @param array $context
     * @return array
     */
    private function expandLocalizedFieldsLines($entries, array $context)
    {
        $result = array();
        foreach ($entries as $entry) {
            $result = array_merge($result, $this->expandLocalizedFieldsLine($entry, $context));
        }
        return $result;
    }

    /**
     * @param $entries
     * @param array $context
     * @return array
     */
    private function expandLocalizedFieldsLine($entries, array $context)
    {
        $locales = $context['localeCodes'];
        $channel = $context['scopeCode'];

        $fields = array();
        foreach ($locales as $locale) {
            foreach ($entries as $key => $entry) {
                if ($idx = strpos($key, sprintf('%s-%s', $locale, $channel))) {
                    $fields[] = substr($key, 0, $idx-1);
                }
            }
        }

        $result = array();
        foreach ($locales as $locale) {
            $items = $entries;
            foreach ($fields as $field) {

                $items[$field] = $entries[sprintf('%s-%s-%s', $field, $locale, $channel)];
                $items['LOCALE'] = $locale;

                foreach ($locales as $localeToRemove) {
                    unset($items[sprintf('%s-%s-%s', $field, $localeToRemove, $channel)]);
                }
            }
            $result[] = $items;
        }

        return $result;
    }

    /**
     * @param $data
     * @return mixed
     */
    private function expandColumns($data)
    {
        foreach ($data as $line => $entries) {
            $result = array();
            foreach ($entries as $column => $entry) {
                if (strpos($column, '%d')) {
                    foreach (explode('|', $entry) as $position => $value) {
                        $result[sprintf($column, $position + 1)] = $value;
                    }
                } else {
                    $result[$column] = $entry;
                }
            }
            $data[$line] = $result;
        }

        return $data;
    }
}
